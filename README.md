# CandyCorn

This is the code for my own version of [Matt Parker's Addressible LED christmas tree light project](https://youtu.be/TvlpIojusBE). It simply lights up a number of LED's (provided by command line arguments) in various segments.

It uses the same WS2811 LED strings as Matt used in the original project, as well as a Raspberry Pi for controlling the color of the LED's.

This is a python program based off of the [same NeoPixels guide](https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage) that Matt Parker used. If something isnt working, those directions may be helpful. 

## Usage
These instructions won't work unless you are on a raspberry pi. 
### Setup

1. install dependencies with `pipenv install`
2. run the project with `sudo $(pipenv --venv)/bin/python3 ./lights.py --leds 50`
3. if it doesnt work, you may need to run the commands at the top of the neopixel directions to make sure the root user has the correct software on the system for operating the GPIO pins. 





