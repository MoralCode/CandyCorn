import unittest
from helpers import segmented_colors_by_segment_count, segmented_colors_by_segment_size

class TestLightsMethods(unittest.TestCase):

    def test_segmented_colors_by_segment_count(self):

        self.assertEqual(
            segmented_colors_by_segment_count([(0,1,2), (3,4,5)], 4),
            [(0,1,2), (0,1,2), (3,4,5), (3,4,5)]
            )

        self.assertEqual(
            segmented_colors_by_segment_count([(0,1,2)], 4),
            [(0,1,2), (0,1,2), (0,1,2), (0,1,2)]
            )


        self.assertEqual( len(segmented_colors_by_segment_count([(0,1,2), (3,4,5), (6,7,8)], 10)), 10 )
        
        # pattern shouldnt wrap
        self.assertEqual(
            segmented_colors_by_segment_count([(0,1,2), (3,4,5), (6,7,8)], 10),
            [(0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (6,7,8), (6,7,8), (0,0,0)]
            )

        self.assertEqual(
            segmented_colors_by_segment_count([(0,1,2)], 50),
            [(0,1,2)] * 50
            )

    def test_segmented_colors_by_segment_size_wrap_by_default(self):

        # wraps by default
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)],3, 10),
            [(0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (6,7,8), (6,7,8), (0,1,2)]
            )

        # disabling wrap
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)],3, 10, pattern_cycles=False),
            [(0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (6,7,8), (6,7,8), (0,0,0)]
            )

    
    def test_segmented_colors_by_segment_size_offset_with_wrap(self):

        # offset by 1
        # tricky because it has to apply the offset before truncating to the number of lights that it has
        # otherwise the start segment may be just a longer string of the same color
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)], 3, 10, offset = 1),
            [(6,7,8), (0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (6,7,8), (6,7,8)]
            )

        # offset by 2
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)], 3, 10, offset = 2),
            [(6,7,8), (6,7,8), (0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (6,7,8)]
            )
        
        # offset by 3
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)], 3, 10, offset = 3),
            [(6,7,8), (6,7,8), (6,7,8), (0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8)]
            )
    

    def test_segmented_colors_by_segment_size_offset_without_wrap(self):

        # offset by 1
        # tricky because it has to apply the offset before truncating to the number of lights that it has
        # otherwise the start segment may be just a longer string of the same color
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)], 3, 10, offset = 1, pattern_cycles=False),
            [(6,7,8), (0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (6,7,8), (0,0,0)]
            )

        # offset by 2
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)], 3, 10, offset = 2, pattern_cycles=False),
            [(6,7,8), (6,7,8), (0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (6,7,8), (0,0,0)]
            )
        
        # offset by 3
        self.assertEqual(
            segmented_colors_by_segment_size([(0,1,2), (3,4,5), (6,7,8)], 3, 10, offset = 3, pattern_cycles=False),
            [(6,7,8), (6,7,8), (6,7,8), (0,1,2), (0,1,2), (0,1,2), (3,4,5), (3,4,5), (3,4,5), (0,0,0)]
            )
        




    

if __name__ == '__main__':
    unittest.main()

