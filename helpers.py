
import math


def segmented_colors_by_segment_count(segments:list, total_lights:int, blank = (0,0,0)):
	"""split the light string into a given number of segments of specific colors

	Args:
		segments (list): a list of each color that you want to assign to each segment
		total_lights (int): the total number of lights to apply the segmentation to.
		blank (tuple): a color to use as a blank color to fill in any excess space. RGB 0,0,0 by default

	Returns: a list of length total_lights containing a color for each light thats either a color from segments or the blank color
	"""
	pixels_per_segment = total_lights / len(segments)
	pixels_per_segment = math.floor(pixels_per_segment)

	return segmented_colors_by_segment_size(segments, pixels_per_segment, total_lights, blank=blank, pattern_cycles=False)

def segmented_colors_by_segment_size(segments:list, segment_size: int, total_lights:int, offset=0,blank = (0,0,0), pattern_cycles=True):
	"""split the light string into segments of a given size and colors

	Args:
		segments (list): a list of each color that you want to assign to each segment
		segment_size (int): the length of each segment
		total_lights (int): the total number of lights to apply the segmentation to.
		offset (int): shift the pattern by this many pixels (can be used to make the pattern move over time)
		blank (tuple): a color to use as a blank color to fill in any excess space. RGB 0,0,0 by default
		pattern_cycles (bool): whether or not to repeat the pattern in the event that it doesnt cover all the lights

	Returns: a list of length total_lights containing a color for each light thats either a color from segments or the blank color
	"""
	# create blank list
	pixel_array = []

	# the total light string is total_lights
	# each set of repeating colors is len(segments)*segment_size long (color_cycle_length)
	color_cycle_length = len(segments)*segment_size
	# there will be math.floor(segment_size/color_cycle_length) complete color cycles
	# there will be total_lights % color_cycle_length leftover lights
	leftover_lights = total_lights % color_cycle_length
	# we should generate math.ceiling(total_lights/color_cycle_length) complete color cycles
	if pattern_cycles:
		cycles_to_fill_lights = math.ceil(total_lights/color_cycle_length)
	else:
		cycles_to_fill_lights = math.floor(total_lights/color_cycle_length)

	# for each complete cycle
	for c in range(cycles_to_fill_lights):
		# for each colored segment
		for segment_color in segments:
			# add some segments of the given length and color
			for p in range(segment_size):
				pixel_array.append(segment_color)


	# apply offset
	if offset > 0:
		pixel_array = pixel_array[-offset:] + pixel_array[:-offset]

	if pattern_cycles:
		# trim the array to match the number of lights we actually have because we overshot until the next whole cycle earlier
		pixel_array = pixel_array[:total_lights]
	else:
		for l in range(leftover_lights):
			pixel_array.append(blank)

	return pixel_array

def rgb_to_grb(rgb:list):
	for (r,g,b) in rgb:
		yield (g,r,b)


def fixed_section(light_array:list, start_index:int, end_index:int, color: (int,int,int)):
	"""set a fixed segment of lights to a specific color

	Args:
		light_array (list): the string of light colors
		start_index (int): the start index - inclusive - of the lights to set to a fixed color
		end_index (int): the end index - exclusive - of the lights to set to a fixed color
		color (int,int,int): the R G and B values of the color to set the segment of lights
	"""
	#shallow copy
	light_array = light_array.copy()

	# TODO: maybe make this into a generator function
	for i in range(start_index, end_index):
		light_array[i] = color
	
	return light_array

	