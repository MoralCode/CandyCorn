# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

# Simple test for NeoPixels on Raspberry Pi
import time
import board
import neopixel
import argparse
from helpers import segmented_colors_by_segment_count, rgb_to_grb, segmented_colors_by_segment_size


parser = argparse.ArgumentParser(description='Some code for a jank LED light display')
parser.add_argument('--leds', type=int,
                    help='the number of LEDs in the string', default= 50)
parser.add_argument('--brightness', type=float,
                    help='the brightness of the LEDs from 0 to 1', default=0.4)
parser.add_argument('--frametime', type=float,
                    help='the time per "frame" of animation of the LEDs in seconds', default=0.2)
parser.add_argument('--mode', choices=['candycorn', 'path'],
                    help='the pattern of the LEDs to display', default='path')

args = parser.parse_args()
# Choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D18
# NeoPixels must be connected to D10, D12, D18 or D21 to work.
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = args.leds

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=args.brightness, auto_write=False, pixel_order=ORDER
)


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)


def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.show()
        time.sleep(wait)



try:
	frame_count = 0
	while True:
		
		#periodically refresh in case something goes wrong
		if args.mode == "candycorn":
			pixel_map = segmented_colors_by_segment_count([(255, 255, 0), (255, 50, 0), (255,255,32)], num_pixels)
		elif args.mode == "path":
			pixel_map = segmented_colors_by_segment_size([(255, 50, 0), (153, 0, 204)], 10, num_pixels, offset=(frame_count % num>

		pixel_map = list(rgb_to_grb(pixel_map))

		for pixel_index in range(len(pixel_map)):
			pixels[pixel_index] = pixel_map[pixel_index]

		pixels.show()
		frame_count += 1
		time.sleep(args.frametime)

		# rainbow_cycle(0.001)  # rainbow cycle with 1ms delay per step
except KeyboardInterrupt:
	pixels.fill((0, 0, 0))
	pixels.show()
